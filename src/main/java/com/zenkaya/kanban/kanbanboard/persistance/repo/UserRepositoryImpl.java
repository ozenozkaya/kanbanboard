package com.zenkaya.kanban.kanbanboard.persistance.repo;

import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import com.zenkaya.kanban.kanbanboard.persistance.entity.UserEntity;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepositoryIface{
    private final UserJpaRepositoryIface userJpaRepositoryIface;
    private final ConverterService converterService;

    @Override
    public UserModel findById(String id) {
        Optional<UserEntity> entity = userJpaRepositoryIface.findById(id);
        return  converterService.convert(entity.get(), UserModel.class);
    }

    @Override
    public UserModel findByName(String name) {
        UserEntity entity = userJpaRepositoryIface.findByName(name);
        return  converterService.convert(entity, UserModel.class);
    }

    @Override
    public List<UserModel> findByNickName(String nickName) {
        List<UserModel> userModels = new ArrayList<UserModel>();
        List<UserEntity> userEntities = userJpaRepositoryIface.findByNickName(nickName);
        for(UserEntity entity : userEntities)
        {
            UserModel model = converterService.convert(entity, UserModel.class);
            userModels.add(model);
        }
        return userModels;
    }

    @Override
    public UserModel create(UserModel user) {
        UserEntity entity =converterService.convert(user, UserEntity.class);
        entity = userJpaRepositoryIface.save(entity);
        return  converterService.convert(entity, UserModel.class);
    }

    @Override
    public UserModel update(UserModel user) {
        UserEntity entity = userJpaRepositoryIface.save(converterService.convert(user, UserEntity.class));
        return  converterService.convert(entity, UserModel.class);
    }

    @Override
    public void delete(String id) {
       userJpaRepositoryIface.deleteById(id);
    }
}
