package com.zenkaya.kanban.kanbanboard.persistance.repo;

import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserRepositoryIface {
    UserModel findById(String id);

    UserModel findByName(String username);

    List<UserModel> findByNickName(String nickName);

    UserModel create(UserModel user);

    UserModel update(UserModel user);

    void delete(String id);
}
