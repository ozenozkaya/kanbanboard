package com.zenkaya.kanban.kanbanboard.persistance.repo;

import com.zenkaya.kanban.kanbanboard.persistance.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface UserJpaRepositoryIface extends JpaRepository<UserEntity, String> {

    Optional<UserEntity> findById(@Param("id") String id);
    UserEntity findByName(String name);
    List<UserEntity> findAll();
    List<UserEntity> findByNickName(String nickName);
}