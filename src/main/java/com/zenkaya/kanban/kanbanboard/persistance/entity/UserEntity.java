package com.zenkaya.kanban.kanbanboard.persistance.entity;

import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import com.zenkaya.kanban.kanbanboard.service.model.Language;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Entity
@NoArgsConstructor
@Table(name = "users")
public class UserEntity extends BaseUserEntity {

    @Builder
    public UserEntity(String id, String name,String surName, String nickName, String eMail, boolean disabled,
                      GenderType genderType, String userIP,
                      LocalDateTime loginTime, LocalDateTime lastActionTime,
                      Language lang) {
        super(id, name, surName, eMail, disabled, lang);
        this.genderType = genderType;
        this.userIP = userIP;
        this.nickName = nickName;
        this.lastActionTime = lastActionTime;
        this.loginTime = loginTime;
    }
    private String nickName;
    private GenderType genderType;
    private String userIP;
    private LocalDateTime lastActionTime;
    private LocalDateTime loginTime;

}
