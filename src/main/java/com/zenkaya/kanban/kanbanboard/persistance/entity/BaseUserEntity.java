package com.zenkaya.kanban.kanbanboard.persistance.entity;

import com.zenkaya.kanban.kanbanboard.service.model.Language;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "base_users")
@Inheritance(strategy = InheritanceType.JOINED)
public class BaseUserEntity {
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
    private String id;
    @NotNull
    private String name;
    @NotNull
    private String surName;
    @NotNull
    private String eMail;
    private boolean disabled;
    @Enumerated(EnumType.STRING)
    private Language lang = Language.EN;

}
