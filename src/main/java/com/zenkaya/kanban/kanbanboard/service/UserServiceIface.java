package com.zenkaya.kanban.kanbanboard.service;

import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserServiceIface {

    UserModel findById(String id);

    UserModel findByName(String name);

    List<UserModel> findByNickName(String nickName);

    Page<UserModel> findAllByName(String name, Pageable pageable);

    UserModel create(UserModel user);

    UserModel update(UserModel user);

    void delete(String id);
}
