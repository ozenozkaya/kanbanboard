package com.zenkaya.kanban.kanbanboard.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import com.zenkaya.kanban.kanbanboard.service.model.Language;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
public class UserModel {
    private String id;
    private String name;
    private String surName;
    private String nickname;
    private String email;
    private GenderType genderType;
    private boolean disabled;
    private boolean active;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastActivityTime;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastLoginTime;
    private Language lang;
    private String userIP;
}
