package com.zenkaya.kanban.kanbanboard.service.impl;

import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import com.zenkaya.kanban.kanbanboard.persistance.repo.UserRepositoryIface;
import com.zenkaya.kanban.kanbanboard.service.UserServiceIface;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserServiceIface {

    private final UserRepositoryIface userRepositoryIface;

    @Override
    public UserModel findById(String id) {
        UserModel user = userRepositoryIface.findById(id);
        user.setLastActivityTime(LocalDateTime.now());

        return userRepositoryIface.update(user);
    }

    @Override
    public UserModel findByName(String name) {

        UserModel user = userRepositoryIface.findByName(name);
        user.setLastActivityTime(LocalDateTime.now());
        return userRepositoryIface.update(user);
    }

    @Override
    public List<UserModel> findByNickName(String name) {

        List<UserModel> userModelList = userRepositoryIface.findByNickName(name);
        for(UserModel model : userModelList)
        {
            model.setLastActivityTime(LocalDateTime.now());
        }

        return userModelList;
    }

    @Override
    public Page<UserModel> findAllByName(String name, Pageable pageable) {
        return null;
    }


    @Override
    public UserModel create(UserModel user) {
        user.setLastActivityTime(LocalDateTime.now());
        user.setLastLoginTime(LocalDateTime.now());
        return userRepositoryIface.create(user);
    }

    @Override
    public UserModel update(UserModel user) {
        user.setLastActivityTime(LocalDateTime.now());
        return userRepositoryIface.update(user);
    }



    @Override
    public void delete(String id) {
        userRepositoryIface.delete(id);
    }
}
