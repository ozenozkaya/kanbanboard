package com.zenkaya.kanban.kanbanboard.service.model;

public enum GenderType {
    MALE("M"),
    FEMALE("F"),
    UNKNOWN("X");
    private String gender;

    GenderType(String newGender) {
        this.gender = newGender;
    }

    public String getString() {
        return this.gender;
    }
}
