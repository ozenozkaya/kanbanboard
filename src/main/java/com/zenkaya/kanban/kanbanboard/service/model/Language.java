package com.zenkaya.kanban.kanbanboard.service.model;

public enum Language {
    EN,
    TR,
    DE,
    PT,
    BR
}