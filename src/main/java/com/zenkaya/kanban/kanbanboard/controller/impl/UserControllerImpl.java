package com.zenkaya.kanban.kanbanboard.controller.impl;

import com.zenkaya.kanban.kanbanboard.controller.UserControllerIface;
import com.zenkaya.kanban.kanbanboard.controller.dto.UserDTO;
import com.zenkaya.kanban.kanbanboard.controller.resource.UserResource;
import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import com.zenkaya.kanban.kanbanboard.service.UserServiceIface;
import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import com.zenkaya.kanban.kanbanboard.service.model.Language;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/v1/users", produces = APPLICATION_JSON_VALUE)
public class UserControllerImpl implements UserControllerIface {

    private final UserServiceIface userServiceIface;
    private final ConverterService converterService;

    @Override
    @GetMapping
    public UserResource getUser( @RequestParam  String userId) {
        UserModel userModel = userServiceIface.findById(userId);
        return converterService.convert(userModel, UserResource.class);
    }

    @Override
    @GetMapping(value = "/byName")
    public UserResource getUserByName(@RequestParam String userName) {
        UserModel userModel = userServiceIface.findByName(userName);
        return converterService.convert(userModel, UserResource.class);
    }

    @Override
    @GetMapping(value = "/byNickName")
    public List<UserResource> getUserByNickName(@RequestParam String nickName) {
        List<UserResource> userResourceList = new ArrayList<>();
        List<UserModel> userModelList = userServiceIface.findByNickName(nickName);
        for(UserModel userModel : userModelList)
        {
            userResourceList.add(converterService.convert(userModel, UserResource.class));
        }
        return userResourceList;
    }

    @Override
    @PutMapping
    public UserResource updateUser(@PathVariable String userId, @RequestBody  UserDTO dto) {

        UserModel userModel = converterService.convert(dto, UserModel.class);
        userModel.setId(userId);
        userModel = userServiceIface.update(userModel);
        return converterService.convert(userModel, UserResource.class);
    }


    @Override
    @DeleteMapping
    public void deleteUser(@RequestParam  String userId) {
        userServiceIface.delete(userId);

    }

    @Override
    @GetMapping(value="/all")
    public Page<UserResource> getUsers(String name, Boolean banned, String language, GenderType genderType, Pageable pageable) {
        return null;
    }

    @Override
    @PostMapping
    public UserResource createUser(@RequestBody  UserDTO user) {
        UserModel userModel = userServiceIface.create(converterService.convert(user, UserModel.class));
        return converterService.convert(userModel, UserResource.class);
    }
}
