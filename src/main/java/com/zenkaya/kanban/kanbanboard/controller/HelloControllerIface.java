package com.zenkaya.kanban.kanbanboard.controller;


import com.zenkaya.kanban.kanbanboard.controller.resource.UserResource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_OK;


@Api(tags = "Hello Controller", description = "Simple Greetings")

public interface HelloControllerIface {


    @ApiOperation("Return Hello")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK", response = String.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    String hello();

}
