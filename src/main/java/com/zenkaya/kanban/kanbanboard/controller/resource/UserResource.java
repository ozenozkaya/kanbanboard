package com.zenkaya.kanban.kanbanboard.controller.resource;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;

import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import com.zenkaya.kanban.kanbanboard.service.model.Language;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserResource {
    private String id;
    private String name;
    private String surName;
    private String nickname;
    private String email;
    private GenderType genderType;
    private boolean disabled;
    private boolean active;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastActivityTime;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime lastLoginTime;
    private Language lang;
    private String userIP;

}