package com.zenkaya.kanban.kanbanboard.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import com.zenkaya.kanban.kanbanboard.service.model.Language;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    @NotEmpty
    private String name;
    @NotEmpty
    private String surName;
    private String nickname;
    private String email;
    private GenderType genderType;
    private boolean disabled;
    private boolean active;
    @NotEmpty
    private Language lang;
}
