package com.zenkaya.kanban.kanbanboard.controller;

import com.zenkaya.kanban.kanbanboard.controller.dto.UserDTO;
import com.zenkaya.kanban.kanbanboard.controller.resource.UserResource;
import com.zenkaya.kanban.kanbanboard.service.model.GenderType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

import static javax.servlet.http.HttpServletResponse.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Api(tags = "User Controller", description = "Operations about user", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public interface UserControllerIface {


    @ApiOperation("Create a user")
    @ApiResponses(value = {
            @ApiResponse(code = SC_CREATED, message = "OK", response = UserResource.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    UserResource createUser(UserDTO user);


    @ApiOperation("Get a user")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK", response = UserResource.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    UserResource getUser(String userId);

    @ApiOperation("Get a user by name")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK", response = UserResource.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    UserResource getUserByName(String userName);


    @ApiOperation("Get a user by nickname")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK", response = UserResource.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    List<UserResource> getUserByNickName(String nickName);


    @ApiOperation("Update a user")
    @ApiResponses(value = {
            @ApiResponse(code = SC_CREATED, message = "OK", response = UserResource.class),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    UserResource updateUser(String userId, UserDTO dto);


    @ApiOperation("Delete a user")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK"),
            @ApiResponse(code = SC_NOT_FOUND, message = "NOT_FOUND", response = String.class)})
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(String userId);

    @ApiOperation("Get all Users")
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "OK", response = UserResource.class)})
    @ResponseStatus(HttpStatus.OK)
    Page<UserResource> getUsers(String name,
                                Boolean banned,
                                String language,
                                GenderType genderType,
                                Pageable pageable);




}
