package com.zenkaya.kanban.kanbanboard.controller.impl;

import com.zenkaya.kanban.kanbanboard.controller.HelloControllerIface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/v1/")
public class HelloControllerImpl implements HelloControllerIface {
    @Override
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello User";
    }
}
