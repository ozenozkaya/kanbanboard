package com.zenkaya.kanban.kanbanboard.config;


import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import java.util.Set;

@Configuration
public class ConverterConfiguration {

    @Bean
    public ConversionService conversionService(Set<Converter> converters) {
        final ConversionServiceFactoryBean factoryBean = new ConversionServiceFactoryBean();
        factoryBean.setConverters(converters);
        factoryBean.afterPropertiesSet();
        return factoryBean.getObject();
    }

    @Bean
    public ConverterService converterService(ConversionService conversionService) {
        return new ConverterService(conversionService);
    }

}
