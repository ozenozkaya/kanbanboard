package com.zenkaya.kanban.kanbanboard.converter.user;

import com.zenkaya.kanban.kanbanboard.controller.resource.UserResource;
import com.zenkaya.kanban.kanbanboard.persistance.entity.UserEntity;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserModelToUserResourceConverter implements Converter<UserModel, UserResource> {
    @Override
    public UserResource convert(UserModel source) {
        return UserResource.builder()
                .disabled(source.isDisabled())
                .email(source.getEmail())
                .genderType(source.getGenderType())
                .id(source.getId())
                .lang(source.getLang())
                .lastActivityTime(source.getLastActivityTime())
                .lastLoginTime(source.getLastLoginTime())
                .name(source.getName())
                .nickname(source.getNickname())
                .surName(source.getSurName())
                .userIP(source.getUserIP())
                .build();
    }
}
