package com.zenkaya.kanban.kanbanboard.converter;


import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;

import java.util.*;

@RequiredArgsConstructor
public class ConverterService {

    private final ConversionService conversionService;

    public <S, T> List<T> convertList(final Iterable<S> sourceList, final Class<T> targetClass) {
        if (sourceList == null) {
            return Collections.emptyList();
        }
        List<T> convertedList = new ArrayList<>();
        sourceList.forEach(o -> convertedList.add(conversionService.convert(o, targetClass)));
        return convertedList;
    }

    public <S, T> Set<T> convertSet(final Iterable<S> sourceSet, final Class<T> targetClass) {
        if (sourceSet == null) {
            return Collections.emptySet();
        }
        Set<T> convertedSet = new HashSet<>();
        sourceSet.forEach(o -> convertedSet.add(conversionService.convert(o, targetClass)));
        return convertedSet;
    }

    public <T> T convert(Object source, Class<T> targetType) {
        return conversionService.convert(source, targetType);
    }
}
