package com.zenkaya.kanban.kanbanboard.converter.user;

import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import com.zenkaya.kanban.kanbanboard.persistance.entity.UserEntity;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserModelToUserEntityConverter implements Converter<UserModel, UserEntity> {

    @Lazy
    @Autowired
    private ConverterService converterService;

    @Override
    public UserEntity convert(UserModel source) {
        return UserEntity.builder()
                .disabled(source.isDisabled())
                .eMail(source.getEmail())
                .genderType(source.getGenderType())
                .id(source.getId())
                .lang(source.getLang())
                .lastActionTime(source.getLastActivityTime())
                .loginTime(source.getLastLoginTime())
                .name(source.getName())
                .nickName(source.getNickname())
                .surName(source.getSurName())
                .userIP(source.getUserIP())
                .build();
    }
}
