package com.zenkaya.kanban.kanbanboard.converter.user;

import com.zenkaya.kanban.kanbanboard.controller.dto.UserDTO;
import com.zenkaya.kanban.kanbanboard.converter.ConverterService;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UserDTOToUserModelConverter implements Converter<UserDTO, UserModel> {

    @Lazy
    @Autowired
    private ConverterService converterService;

    @Override
    public  UserModel convert(UserDTO dto)
    {
        return UserModel.builder()
                .active(dto.isActive())
                .disabled(dto.isDisabled())
                .email(dto.getEmail())
                .genderType(dto.getGenderType())
                .lang(dto.getLang())
                .lastActivityTime(LocalDateTime.now())
                .name(dto.getName())
                .email(dto.getEmail())
                .surName(dto.getSurName())
                .nickname(dto.getNickname())
                .build();
    }
}
