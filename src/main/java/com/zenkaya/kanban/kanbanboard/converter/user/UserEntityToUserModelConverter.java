package com.zenkaya.kanban.kanbanboard.converter.user;

import com.zenkaya.kanban.kanbanboard.persistance.entity.UserEntity;
import com.zenkaya.kanban.kanbanboard.service.model.UserModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserEntityToUserModelConverter implements Converter<UserEntity, UserModel> {
    @Override
    public UserModel convert(UserEntity source) {
        return UserModel.builder()
                .disabled(source.isDisabled())
                .email(source.getEMail())
                .genderType(source.getGenderType())
                .id(source.getId())
                .lang(source.getLang())
                .lastActivityTime(source.getLastActionTime())
                .lastLoginTime(source.getLoginTime())
                .name(source.getName())
                .nickname(source.getNickName())
                .surName(source.getSurName())
                .userIP(source.getUserIP())
                .id(source.getId())
                .build();
    }
}
